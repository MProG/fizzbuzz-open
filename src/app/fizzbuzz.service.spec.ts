import { TestBed, inject } from '@angular/core/testing';

import { FizzbuzzService } from './fizzbuzz.service';

describe('FizzbuzzService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FizzbuzzService]
    });
  });

  it('should be created', inject([FizzbuzzService], (service: FizzbuzzService) => {
    expect(service).toBeTruthy();
  }));
});
