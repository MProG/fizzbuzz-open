import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs/Rx';

@Injectable()
export class FizzbuzzService {

  private interval: Observable<number>;

  constructor() {
    this.interval = Observable.interval(1000);
  }

  public get state (): Observable<number> {
    return this.interval;
  }
}
